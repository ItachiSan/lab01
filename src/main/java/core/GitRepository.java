package core;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

class GitRepository {

	// Respect the class used in the test
	private String path;

	GitRepository(String path) {
		this.path = path;
	}

	private String readFile(String file) throws IOException {
		// Create the file instance
		File f = new File(file);
		// Add its content to the string
		StringBuffer sb = new StringBuffer();
		//String ls = System.getProperty("line.separator");
		InputStreamReader sir = new InputStreamReader(new FileInputStream(f));
		char[] buffer = new char[1024];
		int len;
		while ((len = sir.read(buffer, 0, 1024)) != -1)
			sb.append(new String(buffer, 0, len));
		// ... and then close the reader
		sir.close();
		// Return the output
		return sb.toString();
	}

	String getHeadRef() throws IOException {
		// Read the file
		String headFileContent = readFile(this.path + File.separator + "HEAD");
		/*
		 * Give back the content we need.
		 * The HEAD file is in this format:
		 * ref: SOMETHING
		 * We want that something.
		 */
		return headFileContent.
				// We want the proper stuff, see comments above
				split(": ")[1].
				// Drop the newlines
				replace(System.getProperty("line.separator"), "");
	}

	String getRefHash(String refName) throws IOException {
		// Read the hash from the file
		String refContent = readFile(path + File.separator + refName);
		// Remember to remove the newlines...
		return refContent.replace(System.getProperty("line.separator"), "");
	}
}

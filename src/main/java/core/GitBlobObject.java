package core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.zip.InflaterInputStream;

class GitBlobObject {
	private byte[] content;
	private String hash;
	private String path;
	private String type;
	private int length;

	GitBlobObject(String path, String hash) throws FileNotFoundException, IOException {
		// Keep the hash
		this.hash = hash;
		// Store the path of the object
		this.path = path + File.separator +
				"objects" + File.separator +
				hash.substring(0, 2) + File.separator +
				hash.substring(2);
		// Try to initialize all the fields
		try {
			// Read the inflated file
			InflaterInputStream iir = new InflaterInputStream(
				new FileInputStream(new File(this.path))
			);
			/*
			So, the blob starts after the first "\\x00".
			Let's read the header first!
			We can handle it as a string.
			*/
			int len; // Moved here from code below
			StringBuffer sb = new StringBuffer();
			while ((len = iir.read()) != -1 && len != '\0')
				sb.append((char) len);
			// Now, arrange the header properly!
			String[] header = sb.toString().split(" ");
			this.type = header[0];
			this.length = Integer.parseInt(header[1]);
			// Finally, read all the content and store it
			this.content = new byte[this.length];
			len = iir.read(this.content);
			assert(len == this.length);
			// ... and then close the reader
			iir.close();
		} catch (FileNotFoundException fe) {
			System.out.println("File not found!");
			throw fe;
		} catch (IOException ie) {
			System.out.println("IO Error");
			throw ie;
		}
	}

	String getType() {
		return this.type;
	}

	String getContent() {
		return new String(this.content);
	}

	byte[] getRealContent() {
		return this.content;
	}

	int getLength() {
		return this.length;
	}

	String getHash() {
		return this.hash;
	}
}

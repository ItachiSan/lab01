package core;

import java.io.FileNotFoundException;
import java.io.IOException;

class GitCommitObject extends GitBlobObject {
    /*
    The commit object has some special fields on addition to the normal
    blob type. Qualify them properly.
    */
    private String tree;
    private String parent;
    private String author;
    private String committer;
    private String commit_message;

    GitCommitObject(String path, String hash) throws FileNotFoundException, IOException {
        // Create a blob object now...
        super(path, hash);
        // Then store the proper stuff properly!
        String[] content = this.getContent().split("\n");
        tree = content[0].substring(5);
        parent = content[1].substring(7);
        author = content[2].substring(7);
        committer = content[3].substring(10);
        commit_message = "";
        for(int i = 4; i < content.length; i++)
            commit_message += content[i] + "\n";
    }

    String getTreeHash() {
        return this.tree;
    }

    String getParentHash() {
        return this.parent;
    }

    String getAuthor() {
        // Split at the email end...
        String[] sa = this.author.split("> ");
        // ... and return it properly
        return sa[0] + ">";
    }
}
import zlib
import sys
import os
if os.name == 'nt':
    import win_unicode_console
    win_unicode_console.enable()


if __name__ == '__main__':
    try:
        f = open(sys.argv[1], mode='rb')
        content = zlib.decompress(f.read())
        print(str(content))
        f.close()
    except:
        print(
            "Usage: " +
            sys.argv[0] +
            " <file to deflate>"
        )